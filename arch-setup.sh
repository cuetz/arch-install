#!/bin/bash

# Banner created with DOS Rebel font here: https://manytools.org/hacker-tools/ascii-banner/
# Cleaning the TTY.
clear
echo "                                                                                                                   ";
echo "    █████████                      █████                  █████████            █████                               ";
echo "   ███░░░░░███                    ░░███                  ███░░░░░███          ░░███                                ";
echo "  ░███    ░███  ████████   ██████  ░███████             ░███    ░░░   ██████  ███████   █████ ████ ████████        ";
echo "  ░███████████ ░░███░░███ ███░░███ ░███░░███  ██████████░░█████████  ███░░███░░░███░   ░░███ ░███ ░░███░░███       ";
echo "  ░███░░░░░███  ░███ ░░░ ░███ ░░░  ░███ ░███ ░░░░░░░░░░  ░░░░░░░░███░███████   ░███     ░███ ░███  ░███ ░███       ";
echo "  ░███    ░███  ░███     ░███  ███ ░███ ░███             ███    ░███░███░░░    ░███ ███ ░███ ░███  ░███ ░███       ";
echo "  █████   █████ █████    ░░██████  ████ █████           ░░█████████ ░░██████   ░░█████  ░░████████ ░███████        ";
echo " ░░░░░   ░░░░░ ░░░░░      ░░░░░░  ░░░░ ░░░░░             ░░░░░░░░░   ░░░░░░     ░░░░░    ░░░░░░░░  ░███░░░         ";
echo "                                                                                                   ░███            ";
echo "                                                                                                   █████           ";
echo "                                                                                                  ░░░░░            ";
echo "                                                                                                                   ";
sleep 2

# Pretty print (function).
print () {
    echo -e "\e[1m\e[93m[ \e[92m• \e[39m >>\e[93m ] \e[4m$1\e[0m"
}

# Selecting the target for the installation.
print "Welcome to Arch-Setup (Post-Install script), a script made in order to configure your recent installation of Arch Linux."
PS3="Please select what would you like to add to your base installation: "
select ENTRY in $(lsblk -dpnoNAME|grep -P "/dev/sd|nvme|vd");
do
    DISK=$ENTRY
    print "Installing Arch Linux on $DISK."
    break
done

# Update your mirros
print "Updating mirrors..."
sudo reflector -c Mexico -C US -a 6 --sort rate --save /etc/pacman.d/mirrorlist

# Updating firewall ports:
sudo firewall-cmd --add-port=1025-65535/tcp --permanent
sudo firewall-cmd --add-port=1025-65535/udp --permanent
sudo firewall-cmd --reload

# Audio with pipewire:
print "Installing pipewire for audio:"
sudo pacman -S pipewire gst-plugin-pipewire pipewire-alsa pipewire-jack pipewire-media-session pipewire-pulse pipewire-zeroconf 

# To install AUR packages use the paru AUR manager:
print "Installing AUR package manager (paru):"
git clone https://aur.archlinux.org/paru-bin.git /tmp/paru
cd /tmp/paru
makepkg -si --noconfirm
cd 

#SHELL
print "Installing ZSH Shell:"
sudo pacman -S zsh zsh-autosuggestions zsh-history-substring-search zsh-completions zsh-syntax-highlighting --noconfirm

# Install KDE desktop:
sudo pacman -S plasma-meta sddm kdialog konsole dolphin noto-fonts phonon-qt5-vlc

# Install fonts
print "Installing some fonts:"
sudo pacman -S --noconfirm dina-font xorg-fonts-type1 tamsyn-font bdf-unifont ttf-bitstream-vera ttf-croscore ttf-dejavu ttf-droid gnu-free-fonts ttf-ibm-plex ttf-liberation ttf-linux-libertine noto-fonts ttf-roboto tex-gyre-fonts ttf-ubuntu-font-family ttf-anonymous-pro ttf-cascadia-code ttf-fantasque-sans-mono ttf-fira-mono ttf-hack ttf-fira-code ttf-inconsolata ttf-jetbrains-mono ttf-monofur adobe-source-code-pro-fonts cantarell-fonts inter-font ttf-opensans gentium-plus-font ttf-junicode adobe-source-han-sans-otc-fonts adobe-source-han-serif-otc-fonts noto-fonts-cjk noto-fonts-emoji 

# extra packages, themes and fonts:
print "Adding some extra packages:"
sudo pacman -S inetutils firefox chromium colord-kde kdeconnect exfat-utils

# Instalando QEMU el mejor virtualizador
print "Installing software for virtualization:"
sudo pacman -S qemu qemu-guest-agent qemu-arch-extra qemu-block-gluster qemu-block-iscsi qemu-block-rbd samba edk2-ovmf

# Activamos el servicio para GUEST
systemctl enable --now qemu-guest-agent

# Instalando libvirt
sudo pacman -S virt-viewer virt-manager libvirt ebtables dnsmasq bridge-utils openbsd-netcat

# Activamos servicios de libvirt
systemctl enable --now libvirtd virtlogd
systemctl enable --now virtlogd.socket
systemctl enable --now virtlockd.socket

# Monitor SPICE
sudo pacman -S spice spice-gtk spice-vdagent xf86-video-qxl
systemctl enable --now spice-vdagentd

# Para Ejecutarlo:
# sudo virt-manager

# Install the Docker suite
print "Installing Docker:"
sudo pacman -S docker docker-compose kubectl helm 

